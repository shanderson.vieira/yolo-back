import numpy as np
import pandas as pd
from sklearn.naive_bayes import MultinomialNB
import torch

import json
from fastapi import FastAPI, Depends, Body
from fastapi import responses
from fastapi.responses import JSONResponse
from fastapi import FastAPI, File, UploadFile
from PIL import Image
from io import BytesIO


# Model


model = torch.hub.load('ultralytics/yolov5', 'yolov5s')  # or yolov5m, yolov5x, custom
app = FastAPI()

@app.get("/")
async def raiz():
    return {'mensagem': 'Teste de funcionamento'}


# @app.post("/image_class/")
# async def modelo_yolov5( imagem: UploadFile = File(...)):

#     # Inference
#     conteudo = imagem.file
#     results = model(conteudo)

#     return results.pandas().xyxy[0].to_json(orient="records")

def load_image_into_numpy_array(data):
    return np.array(Image.open(BytesIO(data)))


@app.post("/image_class/")
async def modelo_yolov5( imagem: UploadFile = File(...)):

    image = load_image_into_numpy_array(await imagem.read())

    # Inference
    results = model(image)

    return results.pandas().xyxy[0].to_json(orient="records")