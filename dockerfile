FROM python:3.8-buster

# RUN apt update && apt install -y zip htop screen libgl1-mesa-glx
# RUN apt install -y -q build-essential python3-pip python3-dev

WORKDIR /app

COPY . ./app

# RUN python -m pip install --upgrade pip
# RUN pip uninstall -y nvidia-tensorboard nvidia-tensorboard-plugin-dlprof
# RUN pip install --no-cache -r /app/requirements.txt coremltools onnx gsutil notebook
# RUN pip install --no-cache -U torch torchvision numpy

# RUN pip3 install gunicorn uvloop httptools

#RUN mkdir /app/yolov5

#RUN git clone https://github.com/ultralytics/yolov5 /app/yolov5

#RUN pip3 install -r /app/requirements.txt
#RUN pip3 install -r /app/yolov5/requirements.txt

RUN apt update
RUN apt install libgl1-mesa-glx -y

COPY requirements.txt /app/requirements.txt

RUN pip install -r /app/requirements.txt

RUN pip install python-multipart
RUN pip install fastapi uvicorn

EXPOSE 80

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]
